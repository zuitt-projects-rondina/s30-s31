const User = require('../models/User')

module.exports.createUserController = (req, res) => {

	User.findOne({username : req.body.username}).then(result => {

		if(result !== null && result.username === req.body.username){

			return res.end('Username already taken')

		} else {

			let newUser = new User({

				username : req.body.username,
				password : req.body.password		

			})

			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error))
		}
	})
	.catch(error => res.send(error));
};

module.exports.getAllUserController = (req, res) => {

	User.find({})
	.then(result =>  res.send(result))
	.catch(error => res.send(error))

};

module.exports.updateUserName = (req,res) => {


	let userNameUpdate = {

		username : req.body.username
	}

	User.findByIdAndUpdate(req.params.id, userNameUpdate, {new : true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.getSingleUser = (req, res) => {

	/*let id = req.params.id*/

	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

};