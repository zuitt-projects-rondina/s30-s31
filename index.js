// NOTE : all packages/module should e required at the top of the file to avoid tampering errors

const express = require('express');


// Mongoose is a package that uses an ODM or object document mapper. It allows us to translate our JS objects into database document for MongoDB. It allows conenction and easier manipulation of our documents in MongoDB
const mongoose = require('mongoose');

const taskRoutes = require('./routes/taskRoutes')
const userRoutes = require('./routes/userRoutes')
const port = 4000;

const app = express();


/*
	SYntax:
		mongoose.connect("<connectionStringFromMongoDBAtlas>",{
					userNewUrlParser : true,
					useUnifiedTopology : true,

		})
*/
mongoose.connect("mongodb+srv://admin_rondina:admin169@rondina-b169.sl6hj.mongodb.net/task169?retryWrites=true&w=majority", {

					useNewUrlParser : true,
					useUnifiedTopology : true

});

// CREATE NOTIFICATIONS IF THE CONNECTION TO THE DB IS SUCCESSFUL OR NOT

let db = mongoose.connection;

// We add this so that when db has a connection error , we will show the connection error both in the terminal and in the browser for our client
db.on('error', console.error.bind(console, 'Connection Error'));

// Once the connection is open and successful , we will output a message in the terminal
db.once('open', () => console.log('Connected to MongoDB'));

// middlewares
app.use(express.json());

// our server will use a middleware to group all task routes under /tasks
// All the endpoints in taskRoutes file will start with /tasks
app.use('/tasks' , taskRoutes);
app.use('/users', userRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`))