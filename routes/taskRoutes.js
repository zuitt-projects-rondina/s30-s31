// importing express to use the Router() method
const express = require('express');

// allow us to access our HTTP method routes
const router = express.Router();

// importing taskControllers
const taskControllers = require('../controllers/taskControllers');



// create a task route
router.post('/' , taskControllers.createTaskController);

// retrieving all task route
router.get('/', taskControllers.getAllTasksController);

//retrieving single task routr

router.get('/getSingleTask/:id', taskControllers.getSingleTaskController);

//updating single task status route

router.put('/updateTaskStatus/:id', taskControllers.updateTaskStatusController);



module.exports = router;

