const express = require('express');

const router = express.Router();

const userControllers = require ('../controllers/userControllers')

router.post('/', userControllers.createUserController);

router.get('/', userControllers.getAllUserController);

router.put('/updateUser/:id', userControllers.updateUserName)

router.get('/getSingleUser/:id', userControllers.getSingleUser)

module.exports = router;